import { Component } from '@angular/core';

@Component({
  selector: 'angular-examples-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'todo-ui';
}
