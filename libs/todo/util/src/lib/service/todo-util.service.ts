import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TodoUtilService {
  greeting = () => console.log('Greeting!');
}
