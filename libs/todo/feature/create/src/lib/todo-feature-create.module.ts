import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { todoFeatureCreateRoutes } from './lib.routes';

@NgModule({
  imports: [CommonModule, RouterModule.forChild(todoFeatureCreateRoutes)],
})
export class TodoFeatureCreateModule {}
